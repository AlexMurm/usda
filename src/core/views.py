from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from ext.usda.models import Food, Nutrient

def show_food(request, nbd_no):
    food = get_object_or_404(Food, pk=nbd_no)
    nutrients = Nutrient.objects.filter(food__nbd_no=nbd_no).order_by('nutrient__sr_order')
    proximates = nutrients.filter(nutrient__sr_order__lte = 2200)
    minerals = nutrients.filter(nutrient__sr_order__gte = 2201).filter(nutrient__sr_order__lte = 6240)
    vitamins = nutrients.filter(nutrient__sr_order__gte = 6300).filter(nutrient__sr_order__lte = 8950)
    lipids = nutrients.filter(nutrient__sr_order__gte = 9700).filter(nutrient__sr_order__lte = 16200)
    amino_acids = nutrients.filter(nutrient__sr_order__gte = 16300).filter(nutrient__sr_order__lte = 18100)
    others = nutrients.filter(nutrient__sr_order__gte = 18200)
    return render(request,'admin/show_food.html',{
            'Food':food,
            'proximates':proximates,
            'minerals':minerals,
            'vitamins':vitamins,
            'lipids':lipids,
            'amino_acids':amino_acids,
            'others':others,
    })