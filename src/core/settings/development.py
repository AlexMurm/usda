# -*- coding: utf-8 -*-

from .base import *


DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'content/development.sqlite3'),
    }
}

STATIC_ROOT = os.path.join(BASE_DIR, 'content/static')

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'content/media')

MEDIA_URL = '/media/'
