# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView

from core.views import show_food
urlpatterns = [
    url(r'^admin/usda/show_food/(?P<nbd_no>\d+)/$', show_food, name='show_food'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', RedirectView.as_view(url='/admin/'), name='index'),

]
