# -*- coding: utf-8 -*-

import csv
import datetime
import decimal
import json
import os
import yaml
import zipfile

from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from ext.usda import models


class Command(BaseCommand):
    
    help = 'Imports SR27 data.'
    option_list = BaseCommand.option_list + (
        make_option('--usda', help='The SR27 zipfile', default='sr27.zip'),
        make_option('--db', action='store_true', help='Write to the database'),
        make_option('--json', help='Write to json file. Give the base of the filename. The table name and .json will be added.'),
        make_option('--yaml', help='Write to yaml file. Give the base of the filename. The table name and .yaml will be added.'),
        make_option('--all', action='store_true', help='Import all data.'),
        make_option('--food', action='store_true', help='Import foods.'),
        make_option('--foodgroup', action='store_true', help='Import food groups.'),
        make_option('--nutrientdef', action='store_true', help='Import nutrient definitions.'),
        make_option('--nutrient', action='store_true', help='Import nutrients.'),
        make_option('--weight', action='store_true', help='Import weights.'),
        make_option('--footnote', action='store_true', help='Import footnotes.'),
        make_option('--datasource', action='store_true', help='Import data sources.'),
        make_option('--datasourcelink', action='store_true', help='Import data source links.'),
        make_option('--derivation', action='store_true', help='Import data derivations.'),
        make_option('--langualdef', action='store_true', help='Import langual definitions.'),
        make_option('--langualfactor', action='store_true', help='Import langual factors.'),
        make_option('--datatype', action='store_true', help='Import datatypes.'),
    )

    def do_write(self, f, zip_file, write_db, write_yaml, write_json):
        if write_db:
            f.to_db_bulk(zip_file)
        if write_yaml:
            f.to_yaml(zip_file, '%s_%s.yaml' % (write_yaml, f.tableName))
        if write_json:
            f.to_json(zip_file, '%s_%s.json' % (write_json, f.tableName))

    def handle(self, *args, **options):
        filename = options['usda']
        if not os.path.exists(filename):
            raise CommandError('%s does not exist' % filename)

        write_db = options['db']
        write_yaml = options['yaml']
        write_json = options['json']
        do_all = options['all']

        commands = [
            ('foodgroup', FoodGroupFile()),
            ('nutrientdef', NutrientDefFile()),
            ('derivation', DataDerivationFile()),
            ('langualdef', LanguaLDescriptionFile()),
            ('food', FoodFile()),
            ('datatype', DatatypeFile()),
            ('langualfactor', LanguaLFactorFile()),
            ('nutrient', NutrientFile()),
            ('weight', WeightFile()),
            ('footnote', FootnoteFile()),
            ('datasource', DataSourceFile()),
            ('datasource', DataSourceLinkFile()),
        ]

        with zipfile.ZipFile(filename, mode='r') as zip_file:
            for t in commands:
                if do_all or options[t[0]]:
                    self.do_write(t[1], zip_file, write_db, write_yaml, write_json)


class USDAFile(object):
    
    DB = 1
    JSON = 2
    YAML = 3
    file_name = None
    field_name = ()
    table_name = None
    model = None
    delimiter = '^'
    quote = '~'

    def write(self, t, zipfile, outfile):
        if t == self.DB:
            self.to_db(zipfile)
        elif t == self.JSON:
            self.to_json(zipfile, outfile)
        elif t == self.YAML:
            self.to_yaml(zipfile, outfile)

    def process_row(self, row):
        return row

    def get_zip_data(self, zipfile):
        # data = data.replace(b'\xb5', b'\xc2\xb5')
        # data = data.replace(b'\xe9', b'\xc3\xa9')
        data = zipfile.read(self.file_name)
        data = data.decode(encoding='ISO-8859-1')
        # data = data.encode('UTF-8').decode('UTF-8')
        # print(data)
        data = data.splitlines()
        return data

    def remove_old(self):
        with transaction.atomic():
            # just delete the old ones like this.
            # We can't do the natural self.model.objects.all().delete()
            # because it gives errors (too many sql variables).
            # It also seems that you can't delete in bigger batches either
            count = 0
            while self.model.objects.count():
                ids = self.model.objects.values_list('pk')[:1]
                self.model.objects.filter(pk__in=ids).delete()
                count += 1
            print('Deleted %d records from %s' % (count, self.table_name))

    def to_db_bulk(self, zipfile):
        data = self.get_zip_data(zipfile)
        created = 0
        i = 0
        objs = []

        self.remove_old()

        for row in csv.DictReader(data, self.field_name, delimiter=self.delimiter, quotechar=self.quote):
            row = self.process_row(row)
            objs.append(self.model(**row))
            created += 1
            if i == 10000:
                with transaction.atomic():
                    self.model.objects.bulk_create(objs)
                    objs = []
                    i = 0
                    print('Saved 10000 records of %s. Total %d' % (self.table_name, created))
                transaction.commit()
            i += 1
        
        with transaction.atomic():
            self.model.objects.bulk_create(objs)

        transaction.commit()

        print('Created %d records of %s' % (created, self.table_name))

    def to_db(self, zipfile):
        data = self.get_zip_data(zipfile)
        created = 0
        updated = 0

        with transaction.atomic():
            self.remove_old()
            for row in csv.DictReader(data, self.field_name, delimiter=self.delimiter, quotechar=self.quote):
                row = self.process_row(row)
                o,o_created = self.model.objects.get_or_create(**row)
                if o_created:
                    created += 1
                else:
                    updated += 1

        transaction.commit()
        print('Created %d, updated %d, records of %s' % (created, updated, self.table_name))
        return created, updated

    def to_json(self, zipfile, outfile):
        data = self.get_zip_data(zipfile)
        jsonobjs = []
        for row in csv.DictReader(data, self.field_name, delimiter=self.delimiter, quotechar=self.quote):
            jsonobjs.append({'model': 'usda.' + self.table_name, 'fields': row})

        with open(outfile, 'w') as f:
            json.dump(jsonobjs, f, indent=2)
            print('Created %d json objects of type %s into file %s' % (len(jsonobjs), self.table_name, outfile))

        return len(jsonobjs)

    def to_yaml(self, zipfile, outfile):
        data = self.get_zip_data(zipfile)
        yamlobjs = []
        for row in csv.DictReader(data, self.field_name, delimiter=self.delimiter, quotechar=self.quote):
            yamlobjs.append({'model': 'usda.' + self.table_name, 'fields': row})

        with open(outfile, 'w') as f:
            yaml.dump(yamlobjs, f, indent=2)
            print('Created %d yaml objects of type %s into file %s' % (len(yamlobjs), self.table_name, outfile))

        return len(yamlobjs)

    def to_decimal(self, row, field):
        if row[field] != '':
            row[field] = decimal.Decimal(row[field])
        else:
            row[field] = None

        return row

    def to_int(self, row, field):
        if row[field] != '':
            row[field] = int(row[field])
        else:
            row[field] = None

        return row

    def to_bool(self, row, field):
        if row[field] == 'Y':
            row[field] = True
        else:
            row[field] = None
        return row

    def to_date(self, row, field):
        if row[field] != '':
            m = row[field][0:2]
            y = row[field][3:7]
            row[field] = datetime.date(int(y), int(m), 1)
        else:
            row[field] = None
        return row

    def to_object(self, row, field, model):
        if row[field] != '':
            row[field] = model.objects.get(pk = row[field])
        else:
            row[field] = None
        return row


class FoodGroupFile(USDAFile):
    
    file_name = 'FD_GROUP.txt'
    field_name = ('food_group_code', 'name')
    table_name = 'foodgroup'
    model = models.FoodGroup


class NutrientDefFile(USDAFile):
    
    file_name = 'NUTR_DEF.txt'
    field_name = ('code', 'units', 'tagname', 'name', 'num_decimal_places', 'sr_order')
    table_name = 'nutrientdefinition'
    model = models.NutrientDefinition


class DataDerivationFile(USDAFile):
    
    file_name = 'DERIV_CD.txt'
    field_name = ('code', 'description')
    table_name = 'dataderivation'
    model = models.DataDerivation


class LanguaLDescriptionFile(USDAFile):
    
    file_name = 'LANGDESC.txt'
    field_name = ('code', 'description')
    table_name = 'langualfactordescription'
    model = models.LanguaLFactorDescription


class FoodFile(USDAFile):
    
    file_name = 'FOOD_DES.txt'
    field_name = (
        'nbd_no', 'food_group', 'long_desc', 'short_desc', 'common_name', 'manufacturer_name', 'survey', 'refuse_desc',
        'refuse_percent', 'scientific_name', 'nitrogen_factor', 'protein_factor', 'fat_factor', 'carb_factor'
    )
    table_name = 'food'
    model = models.Food

    def process_row(self, row):
        row = self.to_object(row, 'food_group', models.FoodGroup)
        row = self.to_bool(row, 'survey')
        row = self.to_int(row, 'refuse_percent')
        row = self.to_decimal(row, 'nitrogen_factor')
        row = self.to_decimal(row, 'protein_factor')
        row = self.to_decimal(row, 'fat_factor')
        row = self.to_decimal(row, 'carb_factor')
        return row


class DatatypeFile(USDAFile):
    
    file_name = 'SRC_CD.txt'
    field_name = ('code', 'description')
    table_name = 'datatype'
    model = models.DataType


class LanguaLFactorFile(USDAFile):
    
    file_name = 'LANGUAL.txt'
    field_name = ('food', 'factor')
    table_name = 'langualfactor'
    model = models.LanguaLFactor

    def process_row(self, row):
        row = self.to_object(row, 'food', models.Food)
        row = self.to_object(row, 'factor', models.LanguaLFactorDescription)
        return row


class NutrientFile(USDAFile):
    
    file_name = 'NUT_DATA.txt'
    table_name = 'nutrient'
    model = models.Nutrient
    field_name = (
        'food', 'nutrient', 'amount', 'num_data_points', 'std_error', 'data_type', 'derivation', 'reference_food',
        'added_nutrition', 'num_studies', 'min_value', 'max_value', 'degrees_of_freedom', 'lower_error_bound',
        'upper_error_bound', 'statistical_comments', 'last_modified', 'confidence_code'
    )

    def process_row(self, row):
        row = self.to_object(row, 'food', models.Food)
        row = self.to_object(row, 'nutrient', models.NutrientDefinition)
        row = self.to_object(row, 'data_type', models.DataType)
        row = self.to_object(row, 'derivation', models.DataDerivation)
        row = self.to_object(row, 'reference_food', models.Food)
        row = self.to_bool(row, 'added_nutrition')
        row = self.to_int(row, 'num_data_points')
        row = self.to_int(row, 'num_studies')
        row = self.to_int(row, 'degrees_of_freedom')
        row = self.to_decimal(row, 'std_error')
        row = self.to_decimal(row, 'min_value')
        row = self.to_decimal(row, 'max_value')
        row = self.to_decimal(row, 'amount')
        row = self.to_decimal(row, 'lower_error_bound')
        row = self.to_decimal(row, 'upper_error_bound')
        row = self.to_date(row, 'last_modified')
        return row


class WeightFile(USDAFile):
    
    file_name = 'WEIGHT.txt'
    table_name = 'gramweight'
    model = models.GramWeight
    field_name = ('food', 'sequence', 'amount', 'description', 'weight', 'num_data_points', 'std_deviation')
    next_id = 0
    
    def process_row(self, row):
        row = self.to_object(row, 'food', models.Food)
        row = self.to_int(row, 'num_data_points')
        row = self.to_decimal(row, 'amount')
        row = self.to_decimal(row, 'weight')
        row = self.to_decimal(row, 'std_deviation')
        row['id'] = self.next_id
        self.next_id += 1
        return row


class FootnoteFile(USDAFile):
    
    file_name = 'FOOTNOTE.txt'
    table_name = 'footnote'
    model = models.Footnote
    field_name = ('food', 'sequence', 'type_code', 'nutrient_definition', 'text')
    next_id = 0
    
    def process_row(self, row):
        row = self.to_object(row, 'food', models.Food)
        row = self.to_object(row, 'nutrient_definition', models.NutrientDefinition)
        row['id'] = self.next_id
        self.next_id += 1
        return row


class DataSourceFile(USDAFile):
    
    file_name = 'DATA_SRC.txt'
    table_name = 'datasource'
    model = models.DataSource
    field_name = ('code', 'authors', 'title', 'year', 'journal', 'volume_city', 'issue_state', 'start_page', 'end_page')


class DataSourceLinkFile(USDAFile):
    
    file_name = 'DATSRCLN.txt'
    table_name = 'datasourcelink'
    model = models.DataSourceLink
    field_name = ('food', 'nutrient_definition', 'data_source')

    def process_row(self, row):
        row = self.to_object(row, 'food', models.Food)
        row = self.to_object(row, 'nutrient_definition', models.NutrientDefinition)
        row = self.to_object(row, 'data_source', models.DataSource)
        return row
