# -*- coding: utf-8 -*-

from django.contrib import admin

from ext.usda import models


@admin.register(models.FoodGroup)
class FoodGroupAdmin(admin.ModelAdmin):

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

    list_display = (
        'food_group_code',
        'name',
    )

    search_fields = list_display


@admin.register(models.Food)
class FoodAdmin(admin.ModelAdmin):

    search_fields = (
        'nbd_no',
        'food_group__name',
        'short_desc',
        'common_name',
        'manufacturer_name',
        'survey',
        'refuse_desc',
        'refuse_percent',
        'scientific_name',
        'nitrogen_factor',
        'protein_factor',
        'fat_factor',
        'carb_factor',
    )
    def food_url(self, obj):
        return '<a href="%s">%s</a>' % (obj.get_absolute_url(), obj.short_desc)
    food_url.allow_tags = True

    list_display = (
        'nbd_no',
        'food_group',
        'food_url',
        'common_name',
        'manufacturer_name',
        'survey',
        'refuse_desc',
        'refuse_percent',
        'scientific_name',
        'nitrogen_factor',
        'protein_factor',
        'fat_factor',
        'carb_factor',
    )


@admin.register(models.LanguaLFactorDescription)
class LanguaLFactorDescriptionAdmin(admin.ModelAdmin):

    list_display = (
        'code',
        'description',
    )

    search_fields = list_display


@admin.register(models.LanguaLFactor)
class LanguaLFactorAdmin(admin.ModelAdmin):

    list_display = (
        'food',
        'factor',
    )

    search_fields = list_display


@admin.register(models.DataType)
class DataTypeAdmin(admin.ModelAdmin):

    list_display = (
        'code',
        'description',
    )

    search_fields = list_display


@admin.register(models.DataDerivation)
class DataDerivationAdmin(admin.ModelAdmin):

    list_display = (
        'code',
        'description',
    )

    search_fields = list_display


@admin.register(models.NutrientDefinition)
class NutrientDefinitionAdmin(admin.ModelAdmin):

    list_display = (
        'code',
        'units',
        'tagname',
        'name',
        'num_decimal_places',
        'sr_order',
        'daily_amount',
    )

    search_fields = list_display


@admin.register(models.Nutrient)
class NutrientAdmin(admin.ModelAdmin):

    list_display = (
        'food',
        'nutrient',
        'amount',
        'num_data_points',
        'data_type',
        'derivation',
        'reference_food',
        'added_nutrition',
        'num_studies',
    )


@admin.register(models.GramWeight)
class GramWeightAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'food',
        'sequence',
        'amount',
        'description',
        'weight',
        'num_data_points',
        'std_deviation',
    )

    search_fields = list_display


@admin.register(models.Footnote)
class FootnoteAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'food',
        'sequence',
        'type_code',
        'nutrient_definition',
    )

    search_fields = list_display


@admin.register(models.DataSource)
class DataSourceAdmin(admin.ModelAdmin):

    list_display = (
        'code',
        'title',
        'authors',
        'year',
        'journal',
        'volume_city',
        'issue_state',
        'start_page',
        'end_page',
    )

    search_fields = list_display


@admin.register(models.DataSourceLink)
class DataSourceLinkAdmin(admin.ModelAdmin):

    list_display = (
        'food',
        'nutrient_definition',
        'data_source',
    )

    search_fields = list_display
